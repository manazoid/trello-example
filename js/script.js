const tasksListElement = document.querySelector(`.tasks__list`);
const taskElements = tasksListElement.querySelectorAll(`.tasks__item`);
const deleteArea = document.querySelector("footer");
const textDeleteTasks = document.querySelector(".delete-text-bottom")
const recycleBin = document.querySelector("#recycle-bin");
const titleTable = document.querySelectorAll("h1, h2, h3, li");

// Перебираем все элементы списка и присваиваем нужное значение

for (const task of taskElements) {
    task.draggable = true;
}

tasksListElement.addEventListener(`dragstart`, (evt) => {
    evt.target.classList.add(`selected`);
    textDeleteTasks.classList.add("text-bottom-active");
})

tasksListElement.addEventListener(`dragend`, (evt) => {
    evt.target.classList.remove(`selected`);
    deleteArea.classList.remove("delete-active");
    textDeleteTasks.classList.remove("text-bottom-active");
});

deleteArea.addEventListener("dragover", (evt) => {
    evt.preventDefault()
    deleteArea.classList.add("delete-active");
})

recycleBin.addEventListener("dragover", (evt) => {
    evt.preventDefault();
    evt.target.classList.add("bin-active", "active");
})

recycleBin.addEventListener("drop", (evt) => {
    const activeElement = tasksListElement.querySelector(`.selected`);
    activeElement.remove();
    deleteArea.classList.remove("delete-active");
    textDeleteTasks.classList.remove("text-bottom-active");
    evt.target.classList.remove("bin-active", "active");
})

recycleBin.addEventListener("dragleave", (evt) => {
    evt.target.classList.remove("bin-active", "active");
})

for (const title of titleTable) {
    let inputArea = document.createElement("input");
    title.addEventListener("dblclick", (evt) => {
        let origTxt = evt.target.textContent;

        evt.target.textContent = "";
        evt.target.append(inputArea);
        inputArea.value = origTxt;
        inputArea.focus();
    })
    inputArea.addEventListener("blur", () => {
        if (!inputArea.classList.contains("key-upping")) {
            ReplaceTextTitle(inputArea.value, title);
            inputArea.value = "";
        }
    })

    inputArea.addEventListener("keyup", (evt) => {
        inputArea.classList.add("key-upping");
        if (evt.keyCode == '13') {
            ReplaceTextTitle(inputArea.value, title);
            inputArea.value = "";
        }
        inputArea.classList.remove("key-upping");

    })
}

tasksListElement.addEventListener(`dragover`, (evt) => {
    // Разрешаем сбрасывать элементы в эту область
    evt.preventDefault();
    // Находим перемещаемый элемент
    const activeElement = tasksListElement.querySelector(`.selected`);
    // Находим элемент, над которым в данный момент находится курсор
    const currentElement = evt.target;
    // Проверяем, что событие сработало:
    // 1. не на том элементе, который мы перемещаем,
    // 2. именно на элементе списка
    const isMoveable = activeElement !== currentElement &&
        currentElement.classList.contains(`tasks__item`) && !currentElement.classList.contains("remove-task")

    // Если нет, прерываем выполнение функции
    if (!isMoveable) {
        return;
    }

    // Находим элемент, перед которым будем вставлять
    const nextElement = (currentElement === activeElement.nextElementSibling) ?
        currentElement.nextElementSibling :
        currentElement;

    const getNextElement = (cursorPosition, currentElement) => {
        // Получаем объект с размерами и координатами
        const currentElementCoord = currentElement.getBoundingClientRect();
        // Находим вертикальную координату центра текущего элемента
        const currentElementCenter = currentElementCoord.y + currentElementCoord.height / 2;

        // Если курсор выше центра элемента, возвращаем текущий элемент
        // В ином случае — следующий DOM-элемент
        const nextElement = (cursorPosition < currentElementCenter) ?
            currentElement :
            currentElement.nextElementSibling;

        return nextElement;
    };
    // Вставляем activeElement перед nextElement
    tasksListElement.insertBefore(activeElement, nextElement);
});
tasksListElement.addEventListener(`dragover`, (evt) => {
    evt.preventDefault();

    const activeElement = tasksListElement.querySelector(`.selected`);
    const currentElement = evt.target;
    const isMoveable = activeElement !== currentElement &&
        currentElement.classList.contains(`tasks__item`);

    if (!isMoveable) {
        return;
    }

    // evt.clientY — вертикальная координата курсора в момент,
    // когда сработало событие
    const nextElement = getNextElement(evt.clientY, currentElement);

    // Проверяем, нужно ли менять элементы местами
    if (
        nextElement &&
        activeElement === nextElement.previousElementSibling ||
        activeElement === nextElement
    ) {
        // Если нет, выходим из функции, чтобы избежать лишних изменений в DOM
        return;
    }

    tasksListElement.insertBefore(activeElement, nextElement);
});

// Создаем первый li элемент, это кнопка с надписью "+ добавить элемент"
const elemLi = document.createElement("li");
tasksListElement.append(elemLi)
elemLi.classList.add("remove-task", "tasks__item");

const remove_elem = document.querySelector(".remove-task"),
    el = document.createElement("input");

tasksListElement.append(remove_elem);
remove_elem.draggable = false;
el.maxLength = 20;


// Функция, добавляющая новый элемент в текущий список с учетом введенных данных в Input тег
function ReplaceText(inpt) {
    tmp = inpt.trim();
    if (tmp === "") {
        remove_elem.textContent = "+ Добавить элемент"
        return;
    }
    let create_new_task = document.createElement("li");
    tasksListElement.append(create_new_task);
    create_new_task.textContent = tmp;
    create_new_task.classList.add("tasks__item");
    create_new_task.draggable = true;
    tasksListElement.append(remove_elem);
    remove_elem.textContent = "+ Добавить элемент";
}
function ReplaceTextTitle(inpt, h1) {
    let tmp = inpt.trim();
    // let tag = title.target.parentElement.tagName
    // let tagElem = document.createElement(tag)
    let parentTitle = h1;
    // h1.firstChild.remove();
    if (tmp !== "") {
        // title.parentElement.target.replaceChild(title.target, tagElem);
        // title.textContent = title.target.append(tagElem);
        // tagElem.textContent =  tmp;
        parentTitle.textContent = tmp;
    }
}

// Заменяем текст в поле input
ReplaceText(el.value)
remove_elem.onclick = () => {
    remove_elem.textContent = "";
    remove_elem.append(el);
    el.placeholder = "Введите название";
    el.maxLength = 20;
    el.focus();
}
el.addEventListener("blur", () => {
    if (!el.classList.contains("key-upping")) {
        ReplaceText(el.value);
        el.value = "";
    }
})

el.addEventListener("keyup", (evt) => {
    el.classList.add("key-upping");
    if (evt.keyCode == '13') {
        ReplaceText(el.value);
        el.value = "";
    }
    el.classList.remove("key-upping");
})

// particles-JS
particlesJS("particles-js", {
    "particles": {
        "number": {
            "value": 90,
            "density": {
                "enable": true,
                "value_area": 800
            }
        },
        "color": {
            "value": "#fff"
        },
        "shape": {
            "type": "circle",
            "stroke": {
                "width": 0,
                "color": "#000"
            },
            "polygon": {
                "nb_sides": 5
            },
            "image": {
                "src": "img/github.svg",
                "width": 100,
                "height": 100
            }
        },
        "opacity": {
            "value": 0.5,
            "random": false,
            "anim": {
                "enable": false,
                "speed": 1,
                "opacity_min": 0.1,
                "sync": false
            }
        },
        "size": {
            "value": 3,
            "random": true,
            "anim": {
                "enable": false,
                "speed": 40,
                "size_min": 0.1,
                "sync": false
            }
        },
        "line_linked": {
            "enable": true,
            "distance": 150,
            "color": "#fff",
            "opacity": 0.4,
            "width": 1
        },
        "move": {
            "enable": true,
            "speed": 6,
            "direction": "left",
            "random": false,
            "straight": false,
            "out_mode": "out",
            "bounce": false,
            "attract": {
                "enable": false,
                "rotateX": 600,
                "rotateY": 1200
            }
        }
    },
    "interactivity": {
        "detect_on": "canvas",
        "events": {
            "onhover": {
                "enable": false,
                "mode": "grab"
            },
            "onclick": {
                "enable": false,
                "mode": "push"
            },
            "resize": true
        },
        "modes": {
            "grab": {
                "distance": 140,
                "line_linked": {
                    "opacity": 1
                }
            },
            "bubble": {
                "distance": 400,
                "size": 40,
                "duration": 2,
                "opacity": 8,
                "speed": 3
            },
            "repulse": {
                "distance": 200,
                "duration": 0.4
            },
            "push": {
                "particles_nb": 4
            },
            "remove": {
                "particles_nb": 2
            }
        }
    },
    "retina_detect": true
});


/* ---- stats.js config ---- */

let count_particles, stats, update;
stats = new Stats;
stats.setMode(0);
stats.domElement.style.position = 'absolute';
stats.domElement.style.left = '0px';
stats.domElement.style.top = '0px';
document.body.appendChild(stats.domElement);
count_particles = document.querySelector('.js-count-particles');
update = function () {
    stats.begin();
    stats.end();
    if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) {
        count_particles.innerText = window.pJSDom[0].pJS.particles.array.length;
    }
    requestAnimationFrame(update);
};
requestAnimationFrame(update);

